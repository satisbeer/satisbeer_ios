# Satisbeer_ios

I have been approaching the exercise applying a bit of agile methodologies.
I created User Stories related to the main functionalities of the app. 
The kanban board is present at https://trello.com/b/ba2v3VYm/satisbeerios

### TDD
I wasn't able to apply much of the TDD technique due to the fact that the app was mostly UI. I have created a quick test to handle the Network call.

### Model
The model was created with the json4swift tool.

### MVVM
I have used a simplified version of MVVM.

