//
//  BeerCellViewModelTests.swift
//  SatisBeerTests
//
//  Created by Vitolo, Matteo, Vodafone Italy on 09/08/2019.
//  Copyright © 2019 Vodafone. All rights reserved.
//

import XCTest

class BeerCellViewModelTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInit_empy() {
        let beer = Beer()
        let beercellvm = BeerCellViewModel(beer: beer)
        
        XCTAssert(beercellvm.name == "")
        XCTAssert(beercellvm.description == "")
        XCTAssert(beercellvm.name == "")
        XCTAssert(beercellvm.name == "")

    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
