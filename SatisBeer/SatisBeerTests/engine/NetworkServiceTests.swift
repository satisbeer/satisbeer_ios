//
//  NetworkServiceTests.swift
//  SatisBeerTests
//
//

import XCTest

import Foundation
import Mockingjay

class NetworkServiceTests: XCTestCase {

    var expectations : XCTestExpectation?

    override func setUp() {
        expectations = expectation(description: "AsyncExpectations")
    }
    
    override func tearDown() {
    }

    func testGetBeers(){
    
        let string =  jsonExamples.beersJson
        let url = "https://api.punkapi.com/v2/beers?page=1"
     
        let successTests :( ([Beer]) -> Void) = ({result in
            XCTAssert(true)
            self.expectations?.fulfill()
        })

        let errorTests : ((BeerError) -> Void) = ({result in
            XCTFail()
            self.expectations?.fulfill()
        })
        let serviceBlock : (() -> Void) = ({
            NetworkService().doGetBeers(page:"1", successBlock: successTests, errorBlock: errorTests)
        })
        guard let exp = self.expectations else {
            XCTFail()
            return;
        }
        checkNetworkService(urlToStub: url, jsonToReturn: string, serviceBlock: serviceBlock, expectations: exp)
    }

}
