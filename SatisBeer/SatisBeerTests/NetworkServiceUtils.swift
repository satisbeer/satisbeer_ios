//
//  NetworkServiceUtils.swift
//  SatisBeerTests
//
//
import XCTest
import Mockingjay
import Foundation

extension XCTestCase {
    
    func checkNetworkService(urlToStub:String,jsonToReturn:String, serviceBlock:  @escaping  (() -> Void), expectations:XCTestExpectation) {
        
        expectations.expectedFulfillmentCount = 1
        
        let data = jsonToReturn.data(using: .utf8)!
        do {
            if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [[String:Any]]
            {
                stub(uri(urlToStub), json(jsonArray))
                serviceBlock()
                waitForExpectations(timeout: 2, handler: nil)
            } else {
                XCTFail()
                expectations.fulfill()
            }
        } catch let error as NSError {
            print(error)
            XCTFail()
            expectations.fulfill()
            
        }
    }
}
