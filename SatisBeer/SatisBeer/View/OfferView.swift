//
//  OfferView.swift
//  SatisBeer
//
//

import UIKit

class OfferView: UIView {
    class func instanceFromNib() -> OfferView {
        return UINib(nibName: "OfferView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! OfferView
    }
}
