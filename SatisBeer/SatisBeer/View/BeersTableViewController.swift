//
//  BeersTableViewController.swift
//  SatisBeer
////

import UIKit
import Toast_Swift
import SnapKit

class BeersTableViewController: UITableViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    let viewModel:BeerTableViewModel = BeerTableViewModel()
    
    
    
    var collectionView: UICollectionView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    let searchController = UISearchController(searchResultsController: nil)
    var tableViewHeaderStackView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.backgroundColor = Theme.backgroundBlack
        tableView.register(UINib(nibName: "BeerViewCell", bundle: nil), forCellReuseIdentifier: "BeerViewCell")
        self.navigationItem.titleView = UIUtils.getTitleAttributedString()
        self.tableView.allowsSelection = true

        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        
        navigationItem.searchController = searchController
        definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = false
        tableViewHeaderStackView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 136))   
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 100, height: 25)
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0.0

        self.collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30), collectionViewLayout: layout)
        
        let offerview = OfferView.instanceFromNib()
        
        self.collectionView?.dataSource = self
        self.collectionView?.delegate = self
        collectionView?.backgroundColor = UIColor.clear
        
        tableViewHeaderStackView.addSubview(offerview)
        if let cv = self.collectionView {
            cv.register(BeerTagCell.self, forCellWithReuseIdentifier: "BeerTagCell")
            tableViewHeaderStackView.addSubview(cv)
        }

        offerview.snp.makeConstraints { make in
            make.top.equalTo(tableViewHeaderStackView.snp.top)
            make.leading.equalTo(tableViewHeaderStackView.snp.leading)
            make.trailing.equalTo(tableViewHeaderStackView.snp.trailing)
            make.size.height.equalTo(80)
        }
        collectionView?.snp.makeConstraints { make in
            make.top.equalTo(offerview.snp.bottom).offset(16)
            make.leading.equalTo(tableViewHeaderStackView.snp.leading)
            make.trailing.equalTo(tableViewHeaderStackView.snp.trailing)
            make.size.height.equalTo(30)
        }
        
        self.tableView.tableHeaderView = tableViewHeaderStackView

        self.viewModel.beers.removeAll()
        loadBeers()
    }
    
    func loadMore() {
        self.viewModel.page = self.viewModel.page + 1
        loadBeers();
    }

    func loadBeers(){
        viewModel.loadBeer(successBlock:{
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }, errorBlock: {
            self.view.makeToast("That was a warm beer.")

        })
    }
    
    func searchForBeers(searchText:String?){
        if let searchText = searchText  {
            self.viewModel.beerToSearch = searchText.replacingOccurrences(of: " ", with: "_")
        } else {
            self.viewModel.beerToSearch = ""
        }
        self.viewModel.beers.removeAll()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        self.viewModel.page = 1
        loadBeers()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.beers.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BeerViewCell", for: indexPath) as! BeerViewCell
        cell.updateCell(beer: self.viewModel.beers[indexPath.row])
        cell.openDetailAction = {
            let vc = BeerDetailViewController()
            vc.viewModel = self.viewModel.beers[indexPath.row]
            
            vc.modalPresentationStyle = .overFullScreen
            
            self.present(vc, animated: true, completion: nil)
        }

        // Check if the last row number is the same as the last current data element
        if indexPath.row == self.viewModel.beers.count - 1 {
            self.loadMore()
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 166
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.beerTags.count
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BeerTagCell", for: indexPath) as? BeerTagCell else {
            return UICollectionViewCell()
        }
        
        cell.beerTagTitle.text = viewModel.beerTags[indexPath.row]
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 30)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let toDeselect = (viewModel.beerTags[indexPath.row] == viewModel.selectedBeerTag)
        
        if toDeselect {
            collectionView.deselectItem(at: indexPath, animated: false)
        }
        let beerTag =  toDeselect ? "" : viewModel.beerTags[indexPath.row]
        viewModel.selectedBeerTag = beerTag
        self.searchForBeers(searchText: beerTag)
    }
    
}

extension BeersTableViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        self.searchForBeers(searchText: searchController.searchBar.text)
    }
}

