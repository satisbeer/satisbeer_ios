//
//  BeerDetailViewController.swift
//  SatisBeer
//
//

import UIKit
import SDWebImage

class BeerDetailViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var beerImage: UIImageView!
    @IBOutlet weak var beerDescription: UILabel!
    @IBOutlet weak var beerName: UILabel!
    @IBOutlet weak var beerTagline: UILabel!
    @IBOutlet weak var containerHeight: NSLayoutConstraint!
    @IBOutlet var backgroundView: UIView!
    
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    
    @IBAction func popout(_ sender: Any) {
        self.backgroundView.backgroundColor = UIColor.clear
        self.dismiss(animated: true, completion: nil)
    }
    
    var viewModel:BeerCellViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        containerView.backgroundColor = Theme.backgroundGray
        beerName.textColor = Theme.foregroundWhite
        beerTagline.textColor = Theme.foregroundGray
        beerDescription.textColor = Theme.foregroundGray
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.containerView.frame
        rectShape.position = self.containerView.center
        rectShape.path = UIBezierPath(roundedRect: self.containerView.bounds, byRoundingCorners: [.topRight , .topLeft], cornerRadii: CGSize(width: 20, height: 20)).cgPath
        
        //Here I'm masking the textView's layer with rectShape layer
        self.containerView.layer.mask = rectShape
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3, delay: 0.0, options:[UIView.AnimationOptions.curveEaseInOut, UIView.AnimationOptions.curveEaseInOut], animations: {
            self.backgroundView.backgroundColor = Theme.backgroundGray.withAlphaComponent(0.5)

        }, completion:nil)

    }
    
    func heightForView(text:String) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.beerDescription.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.sizeToFit()
        
        return label.frame.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let vm = viewModel else {
            return
        }
        
        beerName.text = vm.name
        beerDescription.text = vm.description
        beerTagline.text = vm.tagline
        beerImage.sd_setImage(with: URL(string:vm.image_url), completed: nil)
        let height = heightForView(text: vm.description)
        
        self.descriptionHeight.constant = height 
        self.containerHeight.constant = height + 20 + 50 + beerTagline.frame.height + beerName.frame.height
        
    }
}
