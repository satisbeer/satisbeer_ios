//
//  BeerViewCell.swift
//  SatisBeer
//
//

import UIKit
import SDWebImage

class BeerViewCell: UITableViewCell {
    @IBOutlet weak var beerImage: UIImageView!
    @IBOutlet weak var beerTitle: UILabel!
    @IBOutlet weak var beerTagline: UILabel!
    @IBOutlet weak var beerDescription: UILabel!
    
    var openDetailAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.beerTitle.textColor = Theme.foregroundWhite
        self.beerTagline.textColor = Theme.foregroundGray
        self.beerDescription.textColor = Theme.foregroundGray
        
        self.beerTitle.font = Theme.titleFont
        self.beerTagline.font = Theme.titleFont
        self.beerDescription.font = Theme.titleFont
        self.beerImage.contentMode = .scaleAspectFit

        self.backgroundColor = UIColor.clear
    }
    @IBAction func moreInfoAction(_ sender: Any) {
        guard let action = openDetailAction else { return }
        action()
    }
    
    func updateCell(beer: BeerCellViewModel){
        
        self.beerTitle.text = beer.name
        self.beerTagline.text = beer.tagline
        self.beerDescription.text = beer.description
        self.beerImage.sd_setImage(with: URL(string:beer.image_url), completed: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
