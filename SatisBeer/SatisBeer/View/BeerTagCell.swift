//
//  BeerTagCell.swift
//  SatisBeer
//
//

import UIKit
import SnapKit

class BeerTagCell: UICollectionViewCell {
    var beerTagTitle: UILabel = UILabel()
    
    override func layoutSubviews() {
        self.addSubview(beerTagTitle)
        beerTagTitle.snp.makeConstraints { make in
            make.top.equalTo(self.snp.top)
            make.bottom.equalTo(self.snp.bottom)
            make.trailing.equalTo(self.snp.trailing)
            make.leading.equalTo(self.snp.leading)
        }
        
        beerTagTitle.backgroundColor = self.isSelected ? Theme.backgroundDarkYellow : Theme.backgroundGray
        beerTagTitle.textColor = self.isSelected ? Theme.foregroundGray : Theme.foregroundLightGray
        beerTagTitle.textAlignment = .center
        beerTagTitle.layer.cornerRadius = 15
        beerTagTitle.clipsToBounds = true
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override var isSelected: Bool {
        didSet {
            if isSelected {
                self.beerTagTitle.backgroundColor = Theme.backgroundDarkYellow
                self.beerTagTitle.textColor = Theme.foregroundBlack
            } else {
                self.beerTagTitle.backgroundColor = Theme.backgroundGray
                self.beerTagTitle.textColor = Theme.foregroundLightGray

            }
        }
    }
}
