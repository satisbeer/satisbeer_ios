//
//  Theme.swift
//  SatisBeer
//
//

import Foundation
import UIKit

class Theme {
    static let backgroundBlack = UIColor(red:0.04, green:0.09, blue:0.11, alpha:1.0)
    static let foregroundGray = UIColor(red:0.62, green:0.64, blue:0.65, alpha:1.0)
    static let foregroundDarkYellow = UIColor(red:0.99, green:0.69, blue:0.20, alpha:1.0)
    static let backgroundDarkYellow = UIColor(red:0.99, green:0.69, blue:0.20, alpha:1.0)
    static let foregroundBlack = UIColor(red:0.04, green:0.09, blue:0.11, alpha:1.0)
    static let foregroundWhite = UIColor.white
    static let backgroundGray = UIColor(red:0.10, green:0.15, blue:0.17, alpha:1.0)
    static let foregroundLightGray = UIColor(red:0.57, green:0.60, blue:0.60, alpha:1.0)


    static let titleFont = UIFont.systemFont(ofSize: 18)
}
