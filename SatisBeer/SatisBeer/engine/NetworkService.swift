//
//  NetworkService.swift
//  SatisBeer
//
//

import Foundation
import SwiftyJSON

class NetworkService {
    func doGetBeers(page:String, search:String = "", successBlock:@escaping ([Beer])->(), errorBlock:@escaping (BeerError)->()){
        
        let searchToAdd = (search == "") ? "" : "&beer_name=" + search
        
        guard let url = URL(string:"https://api.punkapi.com/v2/beers?page="+page+searchToAdd) else {
            errorBlock(BeerError(code: BeerErrorCode.noUrl, message: "Wrong Url"))
            return
        }
        var request = URLRequest(url:url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error in
            do {
                guard let data = data else {
                        errorBlock(BeerError(code: BeerErrorCode.httpError, message: "Empty Beers... something is pretty wrong!"))
                        return
                }
                let json = try JSON(data: data)
                var list = [Beer]()

                for beerJson in json.arrayValue {
                    if let beerjsonString = beerJson.rawString() {
                        let jsonDecoder = JSONDecoder()
                        if let data = beerjsonString.data(using: .utf8){
                            do {
                                let responseModel = try jsonDecoder.decode(Beer.self, from: data)
                                list.append(responseModel)
                            }catch  {
                                errorBlock(BeerError(code: BeerErrorCode.httpError, message: "Empty Beers... something is pretty wrong!"))
                                return
                            }
                        }
                    }
                }
                successBlock(list)
            } catch {
                errorBlock(BeerError(code: BeerErrorCode.jsonError, message: "Something wrong in those beers..."))

            }
        })
        
        task.resume()
        
    }
}
