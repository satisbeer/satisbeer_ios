//
//  UIUtils.swift
//  SatisBeer
//

import Foundation
import UIKit

class UIUtils {
    static func getTitleAttributedString() -> UILabel {
        let attributedString = NSMutableAttributedString(string: "Beer Box")
        
        let attributes0: [NSAttributedString.Key : Any] = [
            .foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0),
            .font: UIFont(name: "HelveticaNeue", size: 24)!
        ]
        attributedString.addAttributes(attributes0, range: NSRange(location: 0, length: 4))
        
        let attributes1: [NSAttributedString.Key : Any] = [
            .font: UIFont(name: "HelveticaNeue", size: 24)!
        ]
        attributedString.addAttributes(attributes1, range: NSRange(location: 4, length: 1))
        
        let attributes2: [NSAttributedString.Key : Any] = [
            .foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0),
            .font: UIFont(name: "HelveticaNeue-Bold", size: 24)!
        ]
        attributedString.addAttributes(attributes2, range: NSRange(location: 5, length: 3))
        let label = UILabel()
        label.attributedText = attributedString
        return label
    }
}
