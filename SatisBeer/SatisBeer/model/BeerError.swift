//
//  BeerError.swift
//  SatisBeer
//
//

import Foundation

enum BeerErrorCode {
    case noUrl
    case httpError
    case jsonError
};

class BeerError {
    init(code:BeerErrorCode,message:String){
        self.code = code
        self.message = message
        print("Error:" + message)
    }
    var message:String;
    var code:BeerErrorCode;
}
