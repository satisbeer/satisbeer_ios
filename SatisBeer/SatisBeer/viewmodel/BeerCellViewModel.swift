//
//  BeerCellViewModel.swift
//  SatisBeer
//
//

import Foundation

class BeerCellViewModel {
    var name: String
    var tagline: String
    var description: String
    var image_url: String
    
    init(beer: Beer) {
        self.name = beer.name ?? ""
        self.tagline = beer.tagline ?? ""
        self.description = beer.description ?? ""
        self.image_url = beer.image_url ?? ""
    }
}
