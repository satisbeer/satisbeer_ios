//
//  BeerTableViewModel.swift
//  SatisBeer
//
//

import Foundation

class BeerTableViewModel {
    
    var beers:[BeerCellViewModel] = [BeerCellViewModel]()
    var beerTags:[String] = ["Lager", "Blonde", "Malts", "Stout"]
    var selectedBeerTag:String = ""
    var page = 1
    var beerToSearch = ""
    
    func loadBeer(successBlock:@escaping (()->()),errorBlock:@escaping (()->())) {
        DispatchQueue.global(qos: .background).async {
            NetworkService().doGetBeers(page:String(self.page), search: self.beerToSearch , successBlock: {beers in
                for beer in beers {
                    self.beers.append(BeerCellViewModel(beer: beer))
                }
                successBlock()
            }, errorBlock: { error in
                errorBlock()
            })
        }
    }
    
}
